# VBoxer

VBoxer is a script bash for automating installation of virtualbox on debian wheezy or jessie and extention pack.

Features:

-	install dependances
-	adding repository
-	install last version of virtualbox
-	adding user in vboxuser group
-	install extention pack


How to
----

```bash
git clone https://framagit.org/IceF0x/VBoxer.git
cd VBoxer
chmod +x VBoxer
sudo bash VBoxer
```

License
----

* [GPLv3]

[GPLv3]: <http://www.gnu.org/licenses/gpl.txt>